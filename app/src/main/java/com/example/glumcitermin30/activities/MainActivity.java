package com.example.glumcitermin30.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.glumcitermin30.AboutDijalog;
import com.example.glumcitermin30.R;
import com.example.glumcitermin30.db.DatabaseHelper;
import com.example.glumcitermin30.model.Glumac;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;

    EditText et_ime;
    EditText et_prezime;
    EditText et_datum_rodjenja;
    Button btnOk;
    Button btnCancel;

    ArrayAdapter<Glumac> adapter;
    ListView listView;
    List<Glumac> listaGlumaca;

    DatabaseHelper databaseHelper;

    AlertDialog aboutDijalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();
        setList();

    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_name);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }

    private void setList() {

        try {
            listaGlumaca = getDatabaseHelper().getGlumciDao().queryForAll();
            refresh();
        } catch (SQLException e){
            e.printStackTrace();
        }

        if (listaGlumaca == null){
            listaGlumaca = new ArrayList<>();
        }

        listView = findViewById(R.id.list_view);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaGlumaca);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                int glumci_id = adapter.getItem(position).getId();
                intent.putExtra("glumci_id", glumci_id);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_add:
                dodajGlumca();
                break;
            case R.id.settings:
                showSettings();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDijalog() {
        if (aboutDijalog == null) {
            aboutDijalog = new AboutDijalog(this).prepareDialog();
        } else {
            if (aboutDijalog.isShowing()) {
                aboutDijalog.dismiss();
            }
        }
        aboutDijalog.show();
    }

    private void showSettings() {
        Intent intent = new Intent(this, PrefsActivity.class);
        startActivity(intent);
    }

    private void dodajGlumca() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dodaj_glumca);
        dialog.setCancelable(false);

        et_ime = dialog.findViewById(R.id.et_ime);
        et_prezime = dialog.findViewById(R.id.et_prezime);
        et_datum_rodjenja = dialog.findViewById(R.id.et_datum_rodjenja);

        btnCancel = dialog.findViewById(R.id.btnCancel);
        btnOk = dialog.findViewById(R.id.btnOk);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String ime = et_ime.getText().toString().trim();
                if (ime.matches("")) {
                    Toast.makeText(getBaseContext(), "Popunite polje za ime", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!ime.matches("^[a-zA-Z]*$")) {
                    Toast.makeText(getBaseContext(), "Polje za ime prihvata samo slova", Toast.LENGTH_LONG).show();
                    return;
                }
                String prezime = et_prezime.getText().toString().trim();
                if (prezime.matches("")) {
                    Toast.makeText(getBaseContext(), "Popunite polje za prezime", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!ime.matches("^[a-zA-Z]*$")) {
                    Toast.makeText(getBaseContext(), "Polje za prezime prihvata samo slova", Toast.LENGTH_LONG).show();
                    return;
                }
                String datum_rodjenja = et_datum_rodjenja.getText().toString().trim();
                if (datum_rodjenja.matches("")) {
                    Toast.makeText(getBaseContext(), "Popunite polje za datum rodjenja", Toast.LENGTH_SHORT).show();
                    return;
                }

                Glumac glumac = new Glumac();

                glumac.setIme(ime);
                glumac.setPrezime(prezime);
                glumac.setDatum_rodjenja(datum_rodjenja);

                listaGlumaca.add(glumac);

                try {
                    getDatabaseHelper().getGlumciDao().create(glumac);
                    refresh();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void refresh() {
        if (listView != null) {
            adapter = (ArrayAdapter<Glumac>) listView.getAdapter();
            if (adapter != null) {
                try {
                    adapter.clear();
                    listaGlumaca = getDatabaseHelper().getGlumciDao().queryForAll();
                    adapter.addAll(listaGlumaca);
                    adapter.notifyDataSetChanged();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //validacija unosa za filma
    private boolean validacija(EditText editText) {
        String titleInput = editText.getText().toString().trim();

        if (titleInput.isEmpty()) {
            editText.setError("Field can't be empty");
            return false;
        }else {
            editText.setError(null);
            return true;
        }
    }

    @Override
    protected void onResume() {
        refresh();
        super.onResume();
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}
