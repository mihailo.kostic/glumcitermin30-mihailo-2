package com.example.glumcitermin30.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.glumcitermin30.model.Film;
import com.example.glumcitermin30.model.Glumac;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "glumci-treca.db";
    private static final int DATABASE_VERSION = 2;

    public DatabaseHelper (Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private Dao<Glumac, Integer> glumciDao = null;
    private Dao<Film, Integer> filmoviDao = null;


    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, Glumac.class);
            TableUtils.createTable(connectionSource, Film.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {
            TableUtils.dropTable(connectionSource, Glumac.class, true);
            TableUtils.dropTable(connectionSource, Film.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<Glumac, Integer> getGlumciDao() throws SQLException {
        if (glumciDao == null) {
            glumciDao = getDao(Glumac.class);
        }
        return glumciDao;
    }

    public Dao<Film, Integer> getFilmoviDao() throws SQLException{
        if (filmoviDao == null) {
            filmoviDao = getDao(Film.class);
        }
        return filmoviDao;
    }

    @Override
    public void close() {
        glumciDao = null;
        filmoviDao = null;
        super.close();
    }
}
