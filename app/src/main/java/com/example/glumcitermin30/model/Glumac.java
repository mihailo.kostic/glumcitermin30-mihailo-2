package com.example.glumcitermin30.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Glumac.TABLE_NAME_GLUMCI)
public class Glumac {

    public static final String TABLE_NAME_GLUMCI = "glumci";

    public static final String FIELD_GLUMCI_ID = "id";
    public static final String FIELD_GLUMCI_SLIKA = "slika";
    public static final String FIELD_GLUMCI_IME = "ime";
    public static final String FIELD_GLUMCI_PREZIME = "prezime";
    public static final String FIELD_GLUMCI_DATUM_RODJENJA = "datum_rodjenja";

    public static final String TABLE_NAME_FILMOVI = "filmovi";

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(columnName = FIELD_GLUMCI_SLIKA)
    private String imageID;
    @DatabaseField(columnName = FIELD_GLUMCI_IME)
    private String ime;
    @DatabaseField(columnName = FIELD_GLUMCI_PREZIME)
    private String prezime;
    @DatabaseField(columnName = FIELD_GLUMCI_DATUM_RODJENJA)
    private String datum_rodjenja;
    @ForeignCollectionField(foreignFieldName = "glumac", eager = true)
    private ForeignCollection<Film> filmovi;


    public Glumac() {
    }

    public Glumac(String ime, String prezime) {
        this.ime = ime;
        this.prezime = prezime;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageID() {
        return imageID;
    }

    public void setImageID(String imageID) {
        this.imageID = imageID;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getDatum_rodjenja() {
        return datum_rodjenja;
    }

    public void setDatum_rodjenja(String datum_rodjenja) {
        this.datum_rodjenja = datum_rodjenja;
    }

    public ForeignCollection<Film> getFilmovi() {
        return filmovi;
    }

    public void setFilmovi(ForeignCollection<Film> film) {
        this.filmovi = film;
    }



    @Override
    public String toString() {
        return ime + "\n" + prezime;
    }
}
