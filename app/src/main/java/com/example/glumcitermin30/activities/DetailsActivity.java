package com.example.glumcitermin30.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.glumcitermin30.R;
import com.example.glumcitermin30.db.DatabaseHelper;
import com.example.glumcitermin30.model.Film;
import com.example.glumcitermin30.model.Glumac;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {

//    TextView tv_ime_detalji;
    EditText et_ime_detalji;
//    TextView tv_prezime_detalji;
    EditText et_prezime_detalji;
//    TextView tv_datum_rodjenja_detalji;
    EditText et_datum_rodjenja_detalji;

    private Glumac glumac;
    private Film film;
    int glumac_id;
    ArrayAdapter<Film> adapterFilm;
    ListView listViewFilm;
    List<Film> filmLista;

    EditText et_ime_filma;
    EditText et_zanr;
    EditText et_godina_izlaska_filma;
    Button btnOKFilm;
    Button btnCancelFilm;
    ImageButton btn_img_detalji;

    Toolbar toolbar;
    DatabaseHelper databaseHelper;

    private SharedPreferences sharedPreferences;
    public static String TOAST_SETTINGS = "toast_settings_cb";
    public static String NOTIF_SETTINGS = "notif_settings_cb";

    private static final String CHANNEL_ID = "";
    public static final int NOTIF_ID = 1;

    private static final int SELECT_PICTURE = 1;
    private static final String TAG = "Dozvola";
    private String imagePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        setupToolbar();
        setupDetails();
        setupList();
        btn_img_detalji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicture();
                refresh();
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

    }

    private void setupDetails() {

        btn_img_detalji = findViewById(R.id.btn_img_detalji);
        et_ime_detalji = findViewById(R.id.et_ime_detalji);
        et_prezime_detalji = findViewById(R.id.et_prezime_detalji);
        et_datum_rodjenja_detalji = findViewById(R.id.et_datum_rodjenja_detalji);

        int id = getIntent().getExtras().getInt("glumci_id");

        try {
            glumac = getDatabaseHelper().getGlumciDao().queryForId(id);
            refresh();
        } catch (SQLException e) {
            e.printStackTrace();
        }

//        Glumac glumac = new Glumac();  // nemoj ovo koristiti, jer si instancirao novi objekat i zeznuo podatke iz baze kuju pozivas iznad.

        btn_img_detalji.setImageBitmap(BitmapFactory.decodeFile(glumac.getImageID()));
        et_ime_detalji.setText(glumac.getIme());
        et_prezime_detalji.setText(glumac.getPrezime());
        et_datum_rodjenja_detalji.setText(glumac.getDatum_rodjenja());
    }

    private void setupList() {
        listViewFilm = findViewById(R.id.lv_detalji);
        try {
            glumac_id =  getIntent().getIntExtra("glumci_id", 1);
            glumac = getDatabaseHelper().getGlumciDao().queryForId(glumac_id);
            filmLista = getDatabaseHelper().getFilmoviDao().queryForEq("glumac_id",glumac_id);
//            filmLista = getDatabaseHelper().getFilmoviDao().queryForAll();
            for(Film film:filmLista)
                Log.d("REZ", "setupList: " + filmLista);
//            adapterFilm.notifyDataSetChanged();
            refresh();
        } catch (SQLException e) {
            e.printStackTrace();
        }

//        if (filmLista == null) {
//            filmLista = new ArrayList<>();
//        }


        adapterFilm = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, filmLista);
        listViewFilm.setAdapter(adapterFilm);
        adapterFilm.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_add_film:
                dodajFilm();
                Toast.makeText(getBaseContext(), "Dodali ste film", Toast.LENGTH_SHORT).show();
                break;
            case R.id.edit_glumac:
                editGlumac();
                Toast.makeText(getBaseContext(), "Izmenili ste glumca", Toast.LENGTH_SHORT).show();
                break;
            case R.id.delete_glumac:
                deleteGlumac();
                Toast.makeText(getBaseContext(), "Izbrisali ste glumca", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void dodajFilm() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dodaj_film);
        dialog.setCancelable(false);

        et_ime_filma = dialog.findViewById(R.id.etImeFilma);
        et_zanr = dialog.findViewById(R.id.etZanr);
        et_godina_izlaska_filma = dialog.findViewById(R.id.etGodinaIzlaska);

        btnCancelFilm = dialog.findViewById(R.id.btnCancelFilm);
        btnOKFilm = dialog.findViewById(R.id.btnOKfilm);

        btnOKFilm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Film film = new Film();

                if (filmLista != null) {
                    film.setIme(et_ime_filma.getText().toString());
                    film.setZanr(et_zanr.getText().toString());
                    film.setGlumac(glumac);
                    film.setGodina_izlaska(et_godina_izlaska_filma.getText().toString());
                }

                filmLista.add(film);

                try {
                    getDatabaseHelper().getFilmoviDao().createIfNotExists(film);
                    showToast("Film dodat");
//                    showNotif("Film dodat", getApplicationContext());
                    showNotif();
                    refresh();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });

        btnCancelFilm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void editGlumac() {
        if (glumac != null) {
            glumac.setIme(et_ime_detalji.getText().toString());
            glumac.setPrezime(et_prezime_detalji.getText().toString());
//            glumac.setBiografija(etBiografijaDetalji.getText().toString());
//            glumac.setOcena(etOcenaDetalji.getText().toString());
            glumac.setDatum_rodjenja(et_datum_rodjenja_detalji.getText().toString());
            glumac.setImageID(imagePath);

            try {
                getDatabaseHelper().getGlumciDao().update(glumac);
                showToast("Glumac izmenjen");
                showNotif();
//                showNotif("Glumac izmenjen", getApplicationContext());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        onBackPressed();
    }

    private void deleteGlumac() {
        if (glumac != null) {
            try {
                getDatabaseHelper().getGlumciDao().delete(glumac);
                showToast("Glumac obrisan");
//                showNotif("Glumac obrisan", "jhsgdjkh", R.drawable.ic_action_notif);
                showNotif();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        onBackPressed();
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_name);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }

    private void showToast(String message) {
        boolean toast = sharedPreferences.getBoolean(TOAST_SETTINGS, false);
        if (toast) Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Da bi dobili pristup Galeriji slika na uredjaju
     * moramo preko URI-ja pristupiti delu baze gde su smestene
     * slike uredjaja. Njima mozemo pristupiti koristeci sistemski
     * ContentProvider i koristeci URI images/* putanju
     *
     * Posto biramo sliku potrebno je da pozovemo aktivnost koja icekuje rezultat
     * Kada dobijemo rezultat nazad prikazemo sliku i dobijemo njenu tacnu putanju
     * */
    //proverava dozvolu za biranje iz galerije
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED){
            Log.v(TAG,"Permission: "+permissions[0]+ "was "+ grantResults[0]);
        }
    }

    //ako je dozvoljeno bira se slika iz galerije
    private void selectPicture(){
        if (isStoragePermissionGranted()) {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, SELECT_PICTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            if (selectedImage != null) {
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    imagePath = picturePath; //uzima image path i stavlja ga da bude dostupno
                    cursor.close();

                    // String picturePath contains the path of selected Image
//
//                    Dialog dialog = new Dialog(MainActivity.this);
//                    dialog.setContentView(R.layout.image);
//                    dialog.setTitle("Image dialog");

//                    ImageView imageButton = (ImageView) dialog.findViewById(R.id.image);
                    btn_img_detalji.setImageBitmap(BitmapFactory.decodeFile(picturePath));
//
//                    dialog.show();

                    Toast.makeText(this, picturePath,Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

//    private void showNotif(String message, Context context) {
//        boolean notif = sharedPreferences.getBoolean(NOTIF_SETTINGS, false);
//        if (notif) {
//            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
//            builder.setSmallIcon(R.drawable.ic_action_notif);
//            builder.setContentTitle(message);
//            builder.setContentText(message);
//            notificationManager.notify(NOTIF_ID, builder.build());
//        }
//    }

    private void showNotif(){
//        Context context = getApplicationContext();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setContentTitle("Notifikacija")
                .setContentText("Tekst notifikacije")
                .setSmallIcon(R.drawable.ic_action_notif);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "title";
            String description = "desc";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        manager.notify(NOTIF_ID, builder.build());
    }

    private void refresh() {
        if (listViewFilm != null) {
//            adapterFilm = (ArrayAdapter<Film>) listViewFilm.getAdapter();
            if (adapterFilm != null) {
                try {

                    adapterFilm.clear();
                    filmLista = getDatabaseHelper().getFilmoviDao().queryForEq("glumac_id",glumac_id);
                    adapterFilm.addAll(filmLista);
                    adapterFilm.notifyDataSetChanged();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
