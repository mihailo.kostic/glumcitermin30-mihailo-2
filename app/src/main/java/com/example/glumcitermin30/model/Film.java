package com.example.glumcitermin30.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Film.TABLE_NAME_FILMOVI)
public class Film {

    public static final String TABLE_NAME_FILMOVI = "filmovi";

    public static final String FIELD_FILMOVI_ID = "id";
    public static final String FIELD_FILMOVI_IME = "ime";
    public static final String FIELD_FILMOVI_ZANR = "zanr";
    public static final String FIELD_FILMOVI_GODINA_IZLASKA = "godina_izlaska";

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(columnName = FIELD_FILMOVI_IME)
    private String ime;
    @DatabaseField(columnName = FIELD_FILMOVI_ZANR)
    private String zanr;
    @DatabaseField(columnName = FIELD_FILMOVI_GODINA_IZLASKA)
    private String godina_izlaska;
    @DatabaseField(foreignAutoCreate = true, foreign = true, foreignAutoRefresh = true)
    private Glumac glumac;

    public Film() {
    }

    public Film(String ime, String zanr, String godina_izlaska, Glumac glumac) {
        this.ime = ime;
        this.zanr = zanr;
        this.godina_izlaska = godina_izlaska;
        this.glumac = glumac;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getZanr() {
        return zanr;
    }

    public void setZanr(String zanr) {
        this.zanr = zanr;
    }

    public String getGodina_izlaska() {
        return godina_izlaska;
    }

    public void setGodina_izlaska(String godina_izlaska) {
        this.godina_izlaska = godina_izlaska;
    }

    public Glumac getGlumac() {
        return glumac;
    }

    public void setGlumac(Glumac glumac) {
        this.glumac = glumac;
    }

    @Override
    public String toString() {
        return ime + ", " + zanr + ", " + godina_izlaska;
    }
}
